#!/bin/bash
#
# Creates a system image package from the necessary files,
# and also the xml needed to make it fetchable by the SDK Manager

set -e

PROG_DIR=$(dirname $(realpath $0))

FILES="system.img userdata.img kernel-qemu ramdisk.img build.prop NOTICE.txt"
EXTRA_FILES="kernel-ranchu"
XML="sys-img.xml"

TMP_DIR_ROOT=$(mktemp -d -t fdroidsysimg.tmp.XXXXXXXX)
TMP_DIR=$TMP_DIR_ROOT/fdroid-sysimg
mkdir -p $TMP_DIR
trap "rm -rf $TMP_DIR_ROOT" EXIT

function error() {
	echo "*** ERROR: " $@
	usage
}

function usage() {
	cat << EOFU
Usage: $0 inputdir outputdir
where:
- inputdir must contain all of: $FILES
- outputdir will contain the zip and sys-img.xml
EOFU
	exit 1
}

# Parse input
SRC="$1"
[[ -z "$SRC" ]] && error "Missing input directory"

DEST="$2"
[[ -z "$DEST" ]] && error "Missing output directory"
mkdir -p $DEST

# Collect files, from ${1}
# system.img, userdata.img, kernel-qemu, ramdisk.img
# build.prop, NOTICE.txt
for f in $FILES; do
	if [[ ! -f "$SRC"/$f ]]; then
		error "Missing $SRC/$f"
	fi
	cp "$SRC"/$f "$TMP_DIR"/
done

# Optional, kernel-ranchu
for f in $EXTRA_FILES; do
	cp "$SRC"/$f "$TMP_DIR"/
done

# Fill in source.properties - top level
# PLATFORM_SDK_VERSION -> ro.build.version.sdk
# PLATFORM_VERSION -> ro.build.version.sdk
# TARGET_CPU_ABI -> ro.product.cpu.abi
# $1 prop file, $2 prop
function getprop {
	echo `grep "^$2=" $1 | cut -d = -f 2`
}

SDK=$(getprop "$TMP_DIR"/build.prop ro.build.version.sdk)
ABI=$(getprop "$TMP_DIR"/build.prop ro.product.cpu.abi)

cp "$PROG_DIR"/images_source.prop_template "$TMP_DIR"/source.properties
sed -i s/\${PLATFORM_VERSION}/$SDK/g "$TMP_DIR"/source.properties
sed -i s/\${TARGET_CPU_ABI}/$ABI/g "$TMP_DIR"/source.properties
sed -i s/\${PLATFORM_SDK_VERSION}/$SDK/g "$TMP_DIR"/source.properties

ZIP=fdroid_sys-img_${ABI}_${SDK}.zip

# Zip it all up, all under one folder
pushd $TMP_DIR_ROOT
zip -r "$DEST"/$ZIP fdroid-sysimg
popd

# Pass zip to mk_sdk_repo_xml.sh
$PROG_DIR/mk_sdk_repo_xml.sh "$DEST"/$XML "$PROG_DIR"/sdk-sys-img-03.xsd system-image linux "$DEST"/$ZIP

sed -i s#"$DEST"/##g "$DEST"/$XML

# Temporary workaround
# Remove host-os from xml
sed -i /host-os/d "$DEST"/$XML

echo "## Validate XML against schema, again"
xmllint --schema "$PROG_DIR"/sdk-sys-img-03.xsd "$DEST"/$XML
